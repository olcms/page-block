Simple CMS-like app that allows you to dump admin editable blocks 
of markdown into pages via template tag.

# Usage

1. Add to a template tag: 
   
       {% load page_block %}
       (...) 
       
       {% page_block "page-info" %} 
       
2. Go to admin and add "Page Block" with name "page-info" and then add 
   as many translations to it as you want, 
   
   
Block features: 

1. You can override language using ``language=pl`` argument in the tag. 
2. You can set template using ``template="some/template.html"``. 

         