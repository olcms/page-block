"""Apps module"""
# coding=utf-8

from django.apps import AppConfig


class PageBlockConfig(AppConfig):
    """App object for page_block"""
    name = 'page_block'
